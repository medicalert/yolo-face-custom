////////////////////////////////////////////////////////////////////////////////
/// @file YOLOMASK.cpp
/// @brief YOLOMASK face detection wrapper
/// @author Keith Seymour (keithseymour@gmail.com)
///
/// This file contains the implementation of a simple wrapper around the
/// YOLOMASK face detection library.
////////////////////////////////////////////////////////////////////////////////

#include "YOLOMASK.h"

#include "p20_frame_conversion.h"
#include "p20_image_io.h"

#include "opencv2/imgproc.hpp"

/**
 * @brief Create and initialize
 */
pim::YOLOMASK::YOLOMASK()
{
  initialized = false;
}

/**
 * @brief Destroy and clean up
 */
pim::YOLOMASK::~YOLOMASK()
{
}

/**
 * @brief Initialize the underlying face detection system
 *
 * @return true on success, false on failure
 */
bool pim::YOLOMASK::initialize()
{
  initialized = true;
  return initialized;
}

/**
 * @brief Check if already initialized successfully
 *
 * @return true if initialized, false otherwise
 */
bool pim::YOLOMASK::is_initialized() const
{
  return initialized;
}

/**
 * @brief Parse face coordinates
 *
 * The coordinates are expected to be already parsed into the stringlist.
 *
 * @param coords List of coordinates (should be 4 coordinates in the list)
 * @param pt1 on successful return, contains the upper left point
 * @param pt2 on successful return, contains the lower right point
 *
 * @return true on success, false otherwise
 */
bool pim::YOLOMASK::parse_coords(const QStringList &coords,
    cv::Point &pt1, cv::Point &pt2)
{
  if(coords.size() != 4) {
    qDebug()<<"Invalid line: only found "<<coords.size()<<" coordinates";
    return false;
  }

  int cnum[4];

  for(int i=0;i<4;i++) {
    bool parse_ok;

    cnum[i] = coords.at(i).toInt(&parse_ok);

    if(!parse_ok) {
      qDebug()<<"Invalid coordinate: "<<coords.at(i);
      return false;
    }
  }

  pt1 = cv::Point(cnum[0], cnum[1]);
  pt2 = cv::Point(cnum[2], cnum[3]);

  return true;
}

/**
 * @brief Highlight the detected faces in the image
 *
 * @param frame the image to highlight
 * @param coord_file the name of the file containing the coordinates
 * @param ndet on successful return, contains the number of faces detected
 *
 * @return true on success, false otherwise
 */
bool pim::YOLOMASK::highlight_faces(cv::Mat &frame, const QString coord_file,
  int &ndet)
{
  QFile inputFile(coord_file);

  if(!inputFile.open(QIODevice::ReadOnly)) {
    qDebug()<<"Could not open file: "<<coord_file;
    return false;
  }

  ndet = 0;

  QTextStream in(&inputFile);

  while(!in.atEnd()) {
    const QString line = in.readLine();
    const QStringList tokens = line.split(" ", QString::SkipEmptyParts);

    cv::Point pt1, pt2;

    if(!parse_coords(tokens, pt1, pt2))
      return false;

    cv::rectangle(frame, pt1, pt2, cv::Scalar(0, 0, 255), 2);

    ndet++;
  }

  inputFile.close();

  return true;
}

/**
 * @brief Run the specified face detection method
 *
 * @param method The name of the face detection method to use
 * @param src The input image
 * @param dst On successful return, contains the image with faces highlighted
 * @param num_detected On successful return, contains number of faces detected
 * @param coordinates On successful return, contains coordinates of each face
 * @param error_message On failure, contains a descriptive error message
 *
 * @return true on success, false on failure
 */
bool pim::YOLOMASK::detect_faces(const cv::Mat &src, cv::Mat &dst,
  int &num_detected, QString &error_message)
{
#ifdef P20_FD_YOLOMASK_ENABLED
  QTemporaryFile tmp("XXXXXX.png");

  if(!tmp.open()) {
    error_message = "Could not create temp file";
    return false;
  }

  const QString input_tempfile = tmp.fileName();

  if(!pim::opencv_image_io::saveImage_noexc(src, input_tempfile)) {
    error_message = "Failed to save src to temp file";
    return false;
  }

  const QString DARKNET_ROOT = QString("%1/darknet").arg(YOLOMASK_root);

  QProcess proc;

  QStringList arguments;

  arguments << input_tempfile;

  proc.setWorkingDirectory(DARKNET_ROOT);

  proc.start("./run_darknet.sh", arguments);

  if(!proc.waitForStarted()) {
    // Draw error msg on image
    dst = src.clone();
    error_message = "Failed to invoke YOLOMASK";
    return false;
  }

  if(!proc.waitForFinished()) {
    // Draw error msg on image
    dst = src.clone();
    error_message = "YOLOMASK failed";
    return false;
  }

  QProcess::ExitStatus exit_status = proc.exitStatus();
  int exit_code = proc.exitCode();

  if((exit_status == QProcess::NormalExit) && (exit_code == 0)) {
    dst = src.clone();

    if(!highlight_faces(dst, QString("%1/coord.txt").arg(DARKNET_ROOT),
           num_detected))
    {
      error_message = "Could not find and/or process coordinates";
      return false;
    }

    return true;
  }

  if(exit_status == QProcess::NormalExit)
    error_message = QString("YOLOMASK failed with code %1").arg(exit_code);
  else
    error_message = "YOLOMASK crashed";

  return true;
#else
  Q_UNUSED(src);
  Q_UNUSED(dst);

  num_detected = 0;
  error_message = "YOLOMASK not enabled at build time";
  return false;
#endif
}
