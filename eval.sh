#!/bin/bash
echo "Running evaluation on YOLO-Face..."

for folder in /content/WIDER_val/images/*; do
    CHOSEN_FOLDER=$(echo $folder | rev | cut -d'/' -f 1 | rev )
    NEW_FOLDER="/content/WIDER_results/yolo-face/$CHOSEN_FOLDER"
    echo "Creating directory $CHOSEN_FOLDER"
    mkdir $NEW_FOLDER
    for filename in $folder/*; do
        CROPPED_FILE=$(echo $filename | rev | cut -d'.' -f 2 | rev )
        CROPPED_FILE2=$(echo $CROPPED_FILE | rev | cut -d'/' -f 1 | rev )
        NEW_FILE="$NEW_FOLDER/$CROPPED_FILE2.txt"
        ORG_FILE=$(echo $filename | rev | cut -d'/' -f 1 | rev )
        #echo "testtttt $NEW_FILE $ORG_FILE"
	touch $NEW_FILE
	echo "Creating $NEW_FILE"
        echo $ORG_FILE >> $NEW_FILE
        echo "Running in $filename"
	./darknet detect yolo-face.cfg ./yolo-face_500000.weights $filename -out $NEW_FILE
    done
done
